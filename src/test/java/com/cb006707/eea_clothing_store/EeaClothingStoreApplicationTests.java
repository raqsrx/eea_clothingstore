package com.cb006707.eea_clothing_store;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EeaClothingStoreApplicationTests {

	@Test
	public void contextLoads() {
	}

}
